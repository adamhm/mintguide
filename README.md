This is a beginner's guide for [Linux Mint](https://linuxmint.com/) that I originally wrote some years ago and have been updating every so often since as I update my system. Currently updated for Linux Mint 21.3: [Adamhm's Linux Mint 21.3 Beginner's Guide.pdf](https://gitlab.com/adamhm/mintguide/-/raw/master/Adamhm's%20Linux%20Mint%2021.3%20Beginner's%20Guide.pdf?inline=false)

Also check out [my common libraries meta-package for Linux Mint](https://gitlab.com/adamhm/common-libs-meta)

Download Linux Mint: <https://www.linuxmint.com/download.php>
